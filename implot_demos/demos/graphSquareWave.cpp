// Demo:   graph.cpp
// Author: Evan Pezent (evanpezent.com)
// Date:   6/7/2021

#include "App.h"
#include <exprtk.hpp>
#include <iostream>
#include <imgui_stdlib.h>

template <typename T>
static inline T remap(T x, T x0, T x1, T y0, T y1)
{
    return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
}

constexpr static const double pi = (3.141592653589793238462643383279502);
struct Expression1
{

    Expression1()
    {
        table.add_variable("t", t);
        table.add_constant("f", f);
        table.add_constant("a", a);
        table.add_constants();
        expr.register_symbol_table(table);
    }

    bool set(const std::string &_str)
    {
        str = _str;
        return valid = parser.compile(_str, expr);
    }

    double eval(double _t)
    {
        t = _t;
        return expr.value();
    }

    ImVec4 color;
    bool valid;
    std::string str;
    exprtk::symbol_table<double> table;
    exprtk::expression<double> expr;
    exprtk::parser<double> parser;
    const double f = pi / (10);
    const double a = (10);
    double t = (0);
};

struct ImGraphSquareWave : App
{
    Expression1 expr;
    ImPlotRect limits;
    using App::App;

    void Start() override
    {
        const std::string expr_string = "a*(4/pi)*"
                                        "((1 /1)*sin( 2*pi*f*t)+(1 /3)*sin( 6*pi*f*t)+"
                                        " (1 /5)*sin(10*pi*f*t)+(1 /7)*sin(14*pi*f*t)+"
                                        " (1 /9)*sin(18*pi*f*t)+(1/11)*sin(22*pi*f*t)+"
                                        " (1/13)*sin(26*pi*f*t)+(1/15)*sin(30*pi*f*t)+"
                                        " (1/17)*sin(34*pi*f*t)+(1/19)*sin(38*pi*f*t)+"
                                        " (1/21)*sin(42*pi*f*t)+(1/23)*sin(46*pi*f*t)+"
                                        " (1/25)*sin(50*pi*f*t)+(1/27)*sin(54*pi*f*t))";
        expr.set(expr_string);
        expr.color = ImVec4(1, 0.75f, 0, 1);
    }

    void Update() override
    {
        ImGui::SetNextWindowSize(GetWindowSize());
        ImGui::SetNextWindowPos({0, 0});

        ImGui::Begin("ImGraphSquareWave", nullptr, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize);
        bool valid = expr.valid;
        if (!valid)
            ImGui::PushStyleColor(ImGuiCol_FrameBg, {1, 0, 0, 1});
        if (ImGui::InputText("f(x)", &expr.str, ImGuiInputTextFlags_EnterReturnsTrue))
            expr.set(expr.str);
        if (!valid)
            ImGui::PopStyleColor();
        ImGui::SameLine();
        ImGui::ColorEdit4("##Color", &expr.color.x, ImGuiColorEditFlags_NoInputs);

        if (ImPlot::BeginPlot("##Plot", 0, 0, ImVec2(-1, -1), 0, ImPlotAxisFlags_NoInitialFit, ImPlotAxisFlags_NoInitialFit))
        {
            limits = ImPlot::GetPlotLimits();
            if (valid)
            {
                ImPlot::SetupAxesLimits(-10, 10, -15, 15);
                ImPlot::SetNextLineStyle(expr.color);
                ImPlot::PlotLineG(
                    "##item",
                    [](int idx, void *data)
                    {
                        auto &self = *(ImGraphSquareWave *)data;
                        double t = remap((double)idx, 0.0, 9999.0, self.limits.X.Min, self.limits.X.Max);
                        double y = self.expr.eval(t);
                        return ImPlotPoint(t, y);
                    },
                    this,
                    10000);
            }

            ImPlot::EndPlot();
        }
        ImGui::End();
    }
};

int main(int argc, char const *argv[])
{
    ImGraphSquareWave app("ImGraphSquareWave", 640, 480, argc, argv);
    app.Run();
    return 0;
}
